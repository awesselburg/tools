<?php

$configs = new SyncConfigs();

/** add new config with name and path to local git repository */
$magentoConfig = new SyncConfig('Magento', '/Users/awesselburg/debian.dev/__GIT/shopgate/modules/magento');

/** only test mode */
$magentoConfig->setDryRun(true);

/**
 * start add directories
 */
$magentoConfig->addDirectory('app/code/community/Shopgate');
$magentoConfig->addDirectory('app/design/adminhtml/default/default/template/shopgate');
$magentoConfig->addDirectory('app/design/frontend/base/default/template/shopgate');

$magentoConfig->addDirectory('skin/adminhtml/default/default/shopgate');

$magentoConfig->addDirectory('lib/Shopgate');
$magentoConfig->addDirectory('media/shopgate');

/**
 * end add directories
 * start add files
 */

$magentoConfig->addFile('app/design/adminhtml/default/default/layout', 'shopgate.xml');
$magentoConfig->addFile('app/design/frontend/base/default/layout', 'shopgate.xml');

$magentoConfig->addFile('app/etc/modules', 'Shopgate_Framework.xml');

$magentoConfig->addFile('app/locale/de_AT', 'Shopgate_Framework.csv');
$magentoConfig->addFile('app/locale/de_CH', 'Shopgate_Framework.csv');
$magentoConfig->addFile('app/locale/de_DE', 'Shopgate_Framework.csv');

$magentoConfig->addFile('var/connect', 'shopgate_module.xml');

/**
 * end files files
 */

$configs->addConfig($magentoConfig);

/**
 * example for syncFolderTo
 */
$config = new Config('PRESTASHOP', '/Users/awesselburg/debian_jessie/GIT/shopgate/module/prestashop');
$config->setDryRun(false);
$config->setSynToFolder('/modules/shopgate');
$configs->addConfig($config);


class Sync
{
    /** @var Config bool|mixed */
    protected $_config = false;

    /**
     * @param Configs $configs
     */
    public function __construct($configs)
    {
        echo '#################################################' . "\n";
        echo '######## sync.php                        ########' . "\n";
        echo '########                                 ########' . "\n";
        echo '######## awesselburg - wesselburg@me.com ########' . "\n";
        echo '########                                 ########' . "\n";
        echo '#################################################' . "\n";

        if (!count($configs->getConfigs())) {
            die('!!! no config defined !!!');
        }

        $output = $this->_exec(
            sprintf(
                'PS3="select config: "
                    select atype in %s
                    do
                        case $REPLY in
                            %s
                        esac
                    done;',
                $configs->getSelectorLabels(),
                $configs->getSelectorValues()
            )
        );

        $this->_config = $configs->loadById($output[0]);

        $labels = '\\' . "\n" . ' "' . 'SYNC GIT TO PROJECT' . '"';
        $labels = $labels . '\\' . "\n" . ' "' . 'SYNC PROJECT TO GIT' . '"';
        $labels = $labels . '\\' . "\n" . ' "' . 'CLEAN PROJECT' . '"';

        $values = 1 . ' ) echo "' . 1 . '"; exit ;;' . "\n";
        $values = $values . 2 . ' ) echo "' . 2 . '"; exit ;;' . "\n";
        $values = $values . 3 . ' ) echo "' . 3 . '"; exit ;;' . "\n";

        $output = $this->_exec(
            sprintf(
                'PS3="select direction: "
                    select atype in %s
                    do
                        case $REPLY in
                            %s
                        esac
                    done;',
                $labels,
                $values
            )
        );

        $this->_rSync($output[0]);
    }

    /**
     * @return Config|mixed
     */
    protected function _getConfig()
    {
        return $this->_config;
    }

    /**
     * @param int $direction
     */
    protected function _rSync($direction)
    {
        /**
         * handle sync folder
         */
        if ($target = $this->_getConfig()->getSyncFolder()) {
            $command = false;
            switch ($direction[0]) {
                case 1 :
                    /** to source */
                    $this->_prepareDestinationFolder(getcwd(), $target);
                    $source  = $this->_getSourceDir();
                    $command = sprintf(
                        "rsync -r --delete --exclude='.git/' %s/  %s",
                        $source,
                        getcwd() . $target
                    );

                    break;
                case 2 :
                    /** to destination */
                    $command = sprintf(
                        "rsync -r --delete --exclude='.git/' %s/  %s",
                        getcwd() . $target,
                        $this->_getSourceDir()
                    );
                    break;
                case 3 :
                    /** remove project folder */
                    $this->_removeProjectFolder($target, false);
                    $command = false;
                    break;
            }

            $this->_execCommand($command);
        }

        /**
         * handle files
         */
        foreach ($this->_getConfig()->getSyncFiles() as $fileData) {

            $command = false;

            switch ($direction[0]) {
                case 1 :
                    /** to source */
                    switch ($fileData['directory']) {
                        case '/' :
                            $destination = getcwd();
                            $source      = $this->_getSourceFile($fileData);
                            break;
                        default :
                            $source      = $this->_getSourceFile($fileData);
                            $destination = $this->_getProjectDir($fileData['directory']);
                            break;
                    }

                    $command = sprintf(
                        'cp %s %s',
                        $source,
                        $destination
                    );

                    break;
                case 2 :
                    /** to destination */
                    switch ($fileData['directory']) {
                        case '/' :
                            $destination = $this->_getSourceDir();
                            $source      = $this->_getProjectFile($fileData);
                            break;
                        default :
                            $source      = $this->_getProjectFile($fileData);
                            $destination = $this->_getSourceDir($fileData['directory']);
                            break;
                    }

                    $command = sprintf(
                        'cp %s %s',
                        $source,
                        $destination
                    );

                    break;
                case 3 :

                    $file = $this->_getProjectFile($fileData, false);

                    if (is_file($file)) {
                        $command = sprintf(
                            'rm %s',
                            $file
                        );
                    } else {
                        $command = sprintf(
                            '%s already removed',
                            $file
                        );
                    }

                    break;
            }

            $this->_execCommand($command);
        }

        /**
         * handle dirs
         */
        foreach ($this->_getConfig()->getSyncDirs() as $dir) {

            $command = false;

            switch ($direction[0]) {
                case 1 :
                    /** to project */
                    $source      = $this->_getSourceDir($dir);
                    $destination = $this->_prepareDestinationFolder('./', $dir);

                    $command = sprintf(
                        "rsync -r --delete --exclude='.git/' %s  %s",
                        $source,
                        $destination
                    );

                    break;
                case 2 :
                    /** to git */
                    $destination = $this->_prepareDestinationFolder($this->_getSourceDir(), $dir);
                    $source      = $this->_getProjectDir($dir);

                    $command = sprintf(
                        "rsync -r --delete --exclude='.git/' %s  %s",
                        $source,
                        $destination
                    );

                    break;
                case 3 :
                    /** remove project folder */
                    $this->_removeProjectFolder($dir);
                    $command = false;
                    break;
            }

            $this->_execCommand($command);
        }

    }

    /**
     * @param string $command
     */
    protected function _execCommand($command)
    {
        if ($command) {
            $this->_showInfo($command);

            if (!$this->_getConfig()->getDryRun()) {
                $this->_exec($command);
            }
        }
    }

    /**
     * @param string $command
     */
    protected function _showInfo($command)
    {
        if ($this->_getConfig()->getShowInfo()) {
            echo $command . "\n";
        }
    }

    /**
     * @param $rootDir
     * @param $directory
     *
     * @return string
     */
    protected function _prepareDestinationFolder($rootDir, $directory)
    {
        $directories = explode('/', $directory);
        $currentDir  = $rootDir == './' ? '.' : $rootDir;
        $lastDir = false;

        foreach ($directories as $directoryItem) {
            $lastDir    = $currentDir;
            $currentDir = sprintf("%s/%s", $currentDir, $directoryItem);
            if (!is_dir($currentDir)) {
                $this->_showInfo("mkdir(" . $currentDir . ")");
                if (!$this->_getConfig()->getDryRun()) {
                    mkdir($currentDir);
                }
            }
        }

        return $lastDir;
    }

    /**
     * @param           $directory
     * @param bool|true $reverse
     */
    protected function _removeProjectFolder($directory, $reverse = true)
    {
        $directories       = explode('/', $directory);
        $currentDir        = $this->_getProjectDir('./');
        $newDirectoryOrder = array();

        if ($reverse) {
            foreach ($directories as $directoryItem) {
                $currentDir = sprintf("%s/%s", $currentDir, $directoryItem);
                array_push($newDirectoryOrder, $currentDir);
            }
        } else {
            array_push($newDirectoryOrder, $currentDir . $directory);
        }


        foreach (array_reverse($newDirectoryOrder) as $currentDir) {
            if (is_dir($currentDir)) {
                $this->_execCommand(
                    sprintf(
                        'rm -r %s',
                        $currentDir
                    )
                );
            } else {
                $this->_execCommand(
                    sprintf(
                        '%s already removed',
                        $currentDir
                    )
                );
            }
        }
    }

    /**
     * @param bool|false $dir
     * @param bool|true  $needExist
     *
     * @return mixed
     */
    protected function _getSourceDir($dir = false, $needExist = true)
    {
        if (!$dir) {
            return $this->_getDir($this->_getConfig()->getSourceDir(), $needExist);
        } else {
            return $this->_getDir(
                sprintf(
                    "%s/%s",
                    $this->_getConfig()->getSourceDir(),
                    $dir
                ), $needExist
            );
        }
    }

    /**
     * @param           $fileData
     * @param bool|true $needExist
     *
     * @return mixed
     */
    protected function _getSourceFile($fileData, $needExist = true)
    {
        return $this->_getFile(
            sprintf(
                "%s/%s/%s",
                $this->_getSourceDir(),
                $fileData['directory'],
                $fileData['file']
            ), $needExist
        );
    }

    /**
     * @param           $fileData
     * @param bool|true $needExist
     *
     * @return mixed
     */
    public function _getProjectFile($fileData, $needExist = true)
    {
        return $this->_getFile(
            sprintf(
                "%s/%s/%s",
                getcwd(),
                $fileData['directory'],
                $fileData['file']
            ), $needExist
        );
    }

    /**
     * @param           $dir
     * @param bool|true $needExist
     *
     * @return mixed
     */
    protected function _getProjectDir($dir, $needExist = true)
    {
        return $this->_getDir(
            sprintf(
                "%s/%s",
                getcwd(),
                $dir
            ), $needExist
        );
    }

    /**
     * @param           $directory
     * @param bool|true $needExist
     *
     * @return mixed
     */
    protected function _getDir($directory, $needExist = true)
    {
        $directory = $this->_normalizePath($directory);

        if (!is_dir($directory) && $needExist) {
            die('invalid dir ' . $directory);
        }

        return $directory;
    }

    /**
     * @param           $path
     * @param bool|true $needExist
     *
     * @return mixed
     */
    protected function _getFile($path, $needExist = true)
    {
        $path = $this->_normalizePath($path);

        if (!is_file($path) && $needExist) {
            die('invalid file ' . $path);
        }

        return $path;
    }

    /**
     * @param      $command
     * @param bool $info
     *
     * @return mixed
     */
    protected function _exec($command, $info = false)
    {
        exec($command, $output);
        if (is_array($output) && count($output)) {
            if ($info) {
                print_r($info);
            }

            return $output;
        } else {
            return false;
        }
    }

    /**
     * @param $path
     *
     * @return string
     */
    protected function _normalizePath($path)
    {
        $out = array();
        foreach (explode('/', $path) as $i => $fold) {
            if ($fold == '' || $fold == '.') {
                continue;
            }
            if ($fold == '..' && $i > 0 && end($out) != '..') {
                array_pop($out);
            } else {
                $out[] = $fold;
            }
        }

        return ($path{0} == '/' ? '/' : '') . join('/', $out);
    }

}

class Config
{

    /** @var mixed */
    protected $_identifier = false;

    /** @var mixed */
    protected $_sourceDirectory = false;

    /** @var int */
    protected $_id;

    /** @var array */
    protected $_syncDirs = array();

    /** @var array */
    protected $_syncFiles = array();

    /** @var bool */
    protected $_showInfo = true;

    /** @var bool */
    protected $_dryRun = true;

    /**
     * @var mixed
     */
    protected $_syncFolderTarget = false;

    /**
     * @return boolean
     */
    public function getDryRun()
    {
        return $this->_dryRun;
    }

    /**
     * @param boolean $dryRun
     */
    public function setDryRun($dryRun)
    {
        $this->_dryRun = $dryRun;
    }

    /**
     * @return boolean
     */
    public function getShowInfo()
    {
        return $this->_showInfo;
    }

    /**
     * @param boolean $showInfo
     */
    public function setShowInfo($showInfo)
    {
        $this->_showInfo = $showInfo;
    }

    /**
     * @param $identifier
     * @param $sourceDirectory
     */
    public function __construct($identifier, $sourceDirectory)
    {
        $this->_identifier      = $identifier;
        $this->_sourceDirectory = $sourceDirectory;
    }

    /**
     * @return mixed
     */
    public function getSourceDir()
    {
        return $this->_sourceDirectory;
    }

    /**
     * get identifier
     *
     * @return null
     */
    public function getIdentifier()
    {
        return $this->_identifier;
    }

    /**
     * set id
     *
     * @param string $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param $dir
     */
    public function addDirectory($dir)
    {
        array_push(
            $this->_syncDirs,
            $dir
        );
    }

    /**
     * @param $targetFolder
     */
    public function setSynToFolder($targetFolder)
    {
        $this->_syncFolderTarget = $targetFolder;
    }

    /**
     * @return mixed
     */
    public function getSyncFolder()
    {
        return $this->_syncFolderTarget;
    }

    /**
     * @return array
     */
    public function getSyncDirs()
    {
        return $this->_syncDirs;
    }

    /**
     * @param $directory
     * @param $file
     */
    public function addFile($directory, $file)
    {
        array_push(
            $this->_syncFiles,
            array(
                'directory' => $directory,
                'file'      => $file
            )
        );
    }

    /**
     * @return array
     */
    public function getSyncFiles()
    {
        return $this->_syncFiles;
    }

}

class Configs
{
    /**
     * @var array
     */
    protected $_configs = array();

    /**
     * add new config
     *
     * @param Config $config
     */
    public function addConfig(Config $config)
    {
        $config->setId(count($this->getConfigs()) + 1);

        array_push($this->_configs, $config);
    }

    /**
     * get label label selector for shell
     *
     * @return string
     */
    public function getSelectorLabels()
    {
        $result = '';

        /** @var Config $config */
        foreach ($this->_configs as $config) {
            $result = $result . '\\' . "\n" . ' "' . $config->getIdentifier() . '"';
        }

        return $result;
    }

    /**
     * get value selector for shell
     *
     * @return string
     */
    public function getSelectorValues()
    {
        $result = '';

        /** @var Config $config */
        foreach ($this->_configs as $config) {
            $result = $result . $config->getId() . ' ) echo "' . $config->getId() . '"; exit ;;' . "\n";
        }

        return $result;
    }

    /**
     * get configs
     *
     * @return array
     */
    public function getConfigs()
    {
        return $this->_configs;
    }

    /**
     * load config by id
     *
     * @param $id
     *
     * @return Config|mixed
     */
    public function loadById($id)
    {
        foreach ($this->_configs as $config) {
            /** @var Config $config */
            if ($config->getId() == $id) {
                return $config;
            }
        }

        return false;
    }

    /**
     * call deploy
     */
    public function __destruct()
    {
        new Sync($this);
    }
}