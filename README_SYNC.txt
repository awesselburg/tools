Usage

* copy the sync.php in your project root directory
* modify / add config to configs
* call "php sync.php" over your shell

** select your project
** select direction / clean project



** MAGENTO CONFIG EXAMPLE **

$configs = new SyncConfigs();

/** add new config with name and path to local git repository */
$magentoConfig = new SyncConfig('Magento', '/Users/awesselburg/debian.dev/__GIT/shopgate/modules/magento');

/** only test mode */
$magentoConfig->setDryRun(true);

/**
 * start add directories
 */
$magentoConfig->addDirectory('app/code/community/Shopgate');
$magentoConfig->addDirectory('app/design/adminhtml/default/default/template/shopgate');
$magentoConfig->addDirectory('app/design/frontend/base/default/template/shopgate');

$magentoConfig->addDirectory('skin/adminhtml/default/default/shopgate');

$magentoConfig->addDirectory('lib/Shopgate');
$magentoConfig->addDirectory('media/shopgate');

/**
 * end add directories
 * start add files
 */

$magentoConfig->addFile('app/design/adminhtml/default/default/layout', 'shopgate.xml');
$magentoConfig->addFile('app/design/frontend/base/default/layout', 'shopgate.xml');

$magentoConfig->addFile('app/etc/modules', 'Shopgate_Framework.xml');

$magentoConfig->addFile('app/locale/de_AT', 'Shopgate_Framework.csv');
$magentoConfig->addFile('app/locale/de_CH', 'Shopgate_Framework.csv');
$magentoConfig->addFile('app/locale/de_DE', 'Shopgate_Framework.csv');

$magentoConfig->addFile('var/connect', 'shopgate_module.xml');

/**
 * end files files
 */

$configs->addConfig($magentoConfig);

/**
 * example for syncFolderTo
 */
$config = new Config('PRESTASHOP', '/Users/awesselburg/debian_jessie/GIT/shopgate/module/prestashop');
$config->setDryRun(false);
$config->setSynToFolder('/modules/shopgate');
$configs->addConfig($config);

