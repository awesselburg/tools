#!/usr/bin/env bash

SSH_HOSTNAME=5.9.80.213
SSH_USER='root'
DEVCLOUD_DIRECTORY='/var/www/dev/awesselburg'
WORKSPACE_PATH='magento'

WORKSPACE_USER='www-data'
WORKSPACE_GROUP='www-data'

#########################################################################################
#																						#
# FUNCTIONS																				#
#																						#
#########################################################################################

log () {
	echo "#######" $1
}

#########################################################################################
FILE_NAME='magcorp_devcloud.sh'


if [ -n "$1" ] && [ $1 == "remote" ]
	then
	#########################################################################################
	#																						#
	# HANDLE REMOTE																			#
	#																						#
	#########################################################################################

		GIT_HOST=$2
		GIT_BRANCH=$3

		GIT_CHECKOUT_DIR=$DEVCLOUD_DIRECTORY/_git_checkout
		WORKSPACE_DIRECTORY=$DEVCLOUD_DIRECTORY/$WORKSPACE_PATH

		log "START CLEAR GIT CHECKOUT DIRECTORY - $GIT_CHECKOUT_DIR"
		rm -rf $GIT_CHECKOUT_DIR
		log "END CLEAR GIT CHECKOUT DIRECTORY - $GIT_CHECKOUT_DIR"

		log "START CHECKOUT"
		git clone -b $GIT_BRANCH $GIT_HOST $GIT_CHECKOUT_DIR
		log "end CHECKOUT"

		log "START SYNC TO WORKSPACE - $WORKSPACE_DIRECTORY"
		rsync -r --exclude=.git $GIT_CHECKOUT_DIR/ $WORKSPACE_DIRECTORY
		log "END SYNC TO WORKSPACE "

		log "START SET CHOWN / GROUP - $WORKSPACE_USER:$WORKSPACE_GROUP"
		chown -R $WORKSPACE_USER:$WORKSPACE_GROUP $WORKSPACE_DIRECTORY
		log "END SET CHOWN / GROUP"

	else
	#########################################################################################
	#																						#
	# HANDLE LOCAL																			#
	#																						#
	#########################################################################################
		GIT_HOST=`git config --get remote.origin.url`
		GIT_BRANCH=`git symbolic-ref --short -q HEAD`

		if [[ -z "$GIT_HOST" ]]
			then
				exit 1
		fi

		if ssh $SSH_USER@$SSH_HOSTNAME stat $DEVCLOUD_DIRECTORY/$FILE_NAME \> /dev/null 2\>\&1
			then
				log "$FILE_NAME exist on $SSH_HOSTNAME"
			else
				log "START UPLOAD REMOTE SCRIPT"
				scp -q $FILE_NAME $SSH_USER@$SSH_HOSTNAME:$DEVCLOUD_DIRECTORY
				ssh -t -t $SSH_USER@$SSH_HOSTNAME "chmod +x $DEVCLOUD_DIRECTORY/$FILE_NAME"
				log $FILE_NAME "uploaded to" $SSH_HOSTNAME
				log "END UPLOAD REMOTE SCRIPT"
		fi

		scp -q $FILE_NAME $SSH_USER@$SSH_HOSTNAME:$DEVCLOUD_DIRECTORY
		ssh -t -t $SSH_USER@$SSH_HOSTNAME $DEVCLOUD_DIRECTORY/$FILE_NAME remote $GIT_HOST $GIT_BRANCH
fi